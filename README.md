How to play the game?
  See https://youtu.be/Q1biY-LBVYk
  1. Execute SimpleGame.exe in Executable Folder.
  2. Move the hero character with w, a, s, d keys.
  3. Fires tears with the arrow keys.
  4. Move the hero character and step on the footboard to start the boss raid.
  5. Attacks the boss to make the boss's HP zero.

This Project is created by JeongMyeongJoon.